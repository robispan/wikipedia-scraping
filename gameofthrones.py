from urllib2 import urlopen
from bs4 import BeautifulSoup

url = 'https://en.wikipedia.org/wiki/Game_of_Thrones'
response = urlopen(url).read()
soup = BeautifulSoup(response, "html.parser")

# find table with classes "wikitable plainrowheaders"
table = soup.find("table", attrs={'class': 'wikitable plainrowheaders'})

# list of table headings that include season links
lst = table.find_all("th", attrs={'scope': 'row'})

# list of season links
links = []
for item in lst:
    links.append(url[:-21] + item.a['href'])

# list of views per episode
lst_of_views = []
for link in links:
    season_html = urlopen(link).read()
    season_soup = BeautifulSoup(season_html, 'html.parser')

    # find tablerows with class "vevent"
    tr_list = season_soup.find_all("tr", attrs={"class": "vevent"})

    for item in tr_list:
        for i in item.find_all("td")[5:6]:
            string = i.text.partition("[")[0]

            try:
                j = float(string)
                lst_of_views.append(j)
            except ValueError:
                break

views = sum(lst_of_views)

print("\nAccording to wikipedia, Game of Thrones has produced a total of %s mil views to date." % views)
